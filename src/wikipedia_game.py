from config_object import default_config_object, ConfigObject
from sys import argv
from data_objects import LinkObject,AbstractLinkQueue,LinkMessage,LinkStorageObject, LinkQueue, pickle_object,unpickle_object, ProcessCounter
from threading import Thread
from web_utilities import Crawler, create_initial_link, create_subordinate_link
import traceback
import time
from logging_configurator import get_logger

def get_parts_length(val,sep):
    return len(str(val).split(sep))

def pick_shortest(arr, sep):
    curr_choice = None
    curr_length = 0
    for item in arr:
        parts_length = get_parts_length(item,sep)
        if curr_choice is None or parts_length < curr_length:
            curr_choice = item
            curr_length = parts_length

    return curr_choice


class WikiGame(Thread):
    def __init__(self, start_term, end_term, config_object=default_config_object,link_queue=AbstractLinkQueue(), start_time = None):
        super(WikiGame,self).__init__()
        self.config_object = config_object
        self.link_storage = None
        if config_object.use_cache:
            try:
                if config_object.cache_filename is None:
                    self.link_storage = unpickle_object()
                else:
                    self.link_storage = unpickle_object(name=config_object.cache_filename)
            except:
                config_object.debug_print(traceback.format_exc(5))
                self.link_storage = None

        if self.link_storage is None:
            config_object.debug_print("No pickled cache object...using new one (cache?:%s)"%config_object.use_cache)
            self.link_storage= LinkStorageObject()

        self.link_queue = link_queue
        self.crawlers = []
        max = self.config_object.crawler_count + self.config_object.crawler_reverse_count


        self.signal = ProcessCounter(init_count=max)
        self.start_time=start_time
        x = 0
        while x < config_object.crawler_count:
            self.crawlers.append(Crawler(start_term,end_term,crawler_queue=link_queue,link_storage=self.link_storage,config_object=self.config_object,index=x,signal=self.signal, start_time=self.start_time))
            x += 1
        while x < max:
            self.crawlers.append(Crawler(start_term,end_term,crawler_queue=link_queue,link_storage=self.link_storage,config_object=self.config_object,index=x,signal=self.signal, start_time=self.start_time,reverse=True))
            x += 1
        self.start_term = start_term
        self.end_term = end_term


    def do_report(self,silent_on_fail=True):
        end_link = self.link_storage.get(self.end_term)
        if end_link is None:
            if not silent_on_fail:
                print("Term '%s' not found from start point '%s' !!!"%(self.end_term,self.start_term))
            else:
                self.config_object.debug_print("Term '%s' not found from start point '%s' !!!"%(self.end_term,self.start_term))
            return False

        self.config_object.debug_print("%s --> .... --> %s"%(self.start_term,self.end_term))
        arr = end_link.get_paths(top_term=self.start_term,link_trim=config_object.display_trim_length)
        if len(arr) == 1:
            print("arr.length==1:%s"%arr[0])
        else:
            print(pick_shortest(arr, "->"))
            import json
            print(json.dumps(arr))

        return True



    def do_run(self):
        self.config_object.debug_print("Starting run method...")

        cnt=0
        for thrd in self.crawlers:
            if cnt < 4:
                thrd.initial_jitter=0.1
            cnt +=1
            self.config_object.debug_print("Dispatching thread:%s"%thrd.crawlername)
            thrd.start()
            self.config_object.debug_print("Dispatched thread:%s"%thrd.crawlername)
        time.sleep(20)

        self.config_object.debug_print("Entering sleep loop to wait on process")
        while self.signal.get():
            time.sleep(self.config_object.indexer_empty_sleep)

        self.config_object.debug_print("Joining threads to wait for finish...")
        for thrd in self.crawlers:
            try:
                thrd.join()
                self.config_object.debug_print("Joined thread:%s"%thrd.crawlername)

            except:
                self.config_object.debug_print("Error joining thread:%s"%thrd.crawlername)

        self.config_object.debug_print("Calling do report..")
        self.do_report()
        if self.config_object.use_cache:
            self.config_object.debug_print("Pickling link map for cache")
            if self.config_object.cache_filename is not None:
                pickle_object(self.config_object.cache_filename, self.link_storage)
            else:
                pickle_object(object_to_pickle=self.link_storage)
            self.config_object.debug_print("Pickled link map...")
        self.config_object.debug_print("Leaving wikipedia_game :)")

    def run(self):
        try:
            self.do_run()
        except:
            traceback.print_exc()
            try:
                self.signal.notify(term=True)
            except:
                pass


if __name__ == '__main__':
    if len(argv) < 3:
        print("usage: python3 wikipedia_game.py <start_link> <end_link> [.. config:filename.json ]")
        exit(-1)

    start_link = argv[1]
    end_link = argv[2]

    config_object = None
    if len(argv) == 4:
        if argv[3].startswith('config:'):
            cfname = argv[3].split(':')[1]
            config_object = ConfigObject(cfile=cfname)


    if config_object is None:
        config_object = default_config_object
    if config_object.add_default_logger:
        config_object.default_logger = get_logger('WikiGame')
    config_object.debug_print("Loaded config from file:%s"%config_object.filename)
    config_object.debug_print("Creating link from \"%s\" to \"%s\""%(start_link,end_link))
    initial_link =   create_initial_link(start_link, config_object.base_url_format,config_object.base_url_service)
    config_object.debug_print("Using start link --> %s"%initial_link)
    initial_link_obj = LinkObject(url=initial_link, display=start_link)
    initial_link_message = LinkMessage(depth=0, link_object=initial_link_obj)

    link_queue = LinkQueue()
    st = time.time()
    wiki_game = WikiGame(start_term =start_link, end_term=end_link, config_object=config_object,link_queue=link_queue,start_time=st)
    link_queue.append(initial_link_message)

    wiki_game.start()
    wiki_game.join()

    dur = time.time() - st
    print("%2f seconds elapsed for execution..."%dur)

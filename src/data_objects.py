from threading import RLock
import pickle
import traceback

class AtomicCounter(object):
    def __init__(self, init_value=0):
        self.lock = RLock()
        self.count = init_value


    def increment(self, num=1):
        with self.lock:
            self.count += num

    def get(self):
        val=0
        with self.lock:
            val = self.count
        return val


class Averager(object):
    def __init__(self):
        self.lock = RLock()
        self.total=0.0
        self.count=0
        self.computed_average = 0.0

    def average(self):
        with self.lock:
            return self.computed_average

    def add_value(self, val):
        with self.lock:
            self.total += float(val)
            self.count += 1
            self.computed_average = self.total / float(self.count)

    def get_and_clear(self):
        ret = None
        with self.lock:
            ret = self.count
            self.count = 0
        return ret

class ProcessCounter(object):
    def __init__(self, init_count=1):
        self.values = []
        x=0
        while x < init_count:
            self.values.append(False)
            x+=1
        self.lock=RLock()


    def notify(self,term=False):
        with self.lock:
            print("Notify called:%s"%traceback.format_stack(limit=2))
            if term:
                self.values.clear()
            elif len(self.values) > 0:
                self.values.pop()

    def count(self):
        with self.lock:
            return len(self.values)

    def get(self):
        return (self.count() > 0)


class AbstractLinkQueue(object):
    def __init__(self):
        self.lock = RLock()

    def add_to_queue(self, func_ob, signal=True):
        pass

    def append(self, func_ob):
        self.add_to_queue(func_ob)

    def pop_from_queue(self,random=False, ascending=True):
        return None

    def empty(self):
        return True

    def size(self):
        return 0

    def count_and_empty(self):
        return self.size(), self.empty()


class LinkObject(object):

    def __init__(self, url = None, display = None, inbound=None, inbound_objects=None):
        self.url = url
        self.display = display
        if inbound is None:
            self.inbound = set()
        else:
            self.inbound = inbound

        if inbound_objects is None:
            self.inbound_objects=set()
        else:
            self.inbound_objects = inbound_objects

        self.lock = RLock()
        self.link_valid = True
        self.child_links = set()
        self.set_lock = RLock()

    def add_child(self, child_display):
        with self.set_lock:
            self.child_links.add(child_display)

    def contains_child(self, child_display):
        ret = False
        with self.set_lock:
            ret = (child_display in self.child_links)
        return ret

    def size(self):
        sz = 0
        with self.set_lock:
            sz = len(self.child_links)
        return sz

    def add_inbound(self, inbound_object, storage=None):
        added = False
        message = None
        if inbound_object is None:
            message="[%s] inbound object None"%self.display
        elif inbound_object.display is None:
            message= "[%s] inbound_object.display is None"%self.display

        elif self.display != inbound_object.display:
            with self.lock:
                if self.search_for_parent_key(inbound_object.display):
                    added=False
                    message="[%s] Child object(%s) exists in lineage(parent)" %(self.display,inbound_object.display)
                else:
                    self.inbound.add(inbound_object.display)
                    self.inbound_objects.add(inbound_object)

                    added = True
        else:
            message="[%s] inbound_object.display matched self"%self.display

        return added, message

    def get_ordered_inbound_string_list(self):
        arr = []
        for item in self.inbound:
            arr.append(item)
        arr.sort()
        return arr

    def get_ordered_inbound_list(self):
        arr = []
        for item in self.inbound_objects:
            arr.append(item)
        arr.sort(key = lambda x:x.display)
        return arr

    def get_paths(self,top_term=None,link_trim=None):
        if link_trim is not None:
            try:
                link_trim = int(link_trim)
            except:
                #not an int
                link_trim = None

        #print("%s --> get_paths"%self.display)
        arr = self.get_ordered_inbound_list()
        if link_trim is not None and self.display is not None and len(self.display) > link_trim:
            printed_display = "%s ..."%self.display[0:link_trim]
        else:
            printed_display = self.display

        if  self.display == top_term or len(arr) == 0:
            return [printed_display]

        results = []
        for item in arr:
            inbound_arr = item.get_paths(top_term=top_term,link_trim=link_trim)
            for ib_item in inbound_arr:
                results.append("%s --> %s"%(ib_item,printed_display))
        #import json
        #print("results:%s"%json.dumps(results))
        return results

    def get_parent_set(self):
        parent_set = set()
        for item in self.inbound_objects:
            parent_set.add(item.display.lower())
            parent_set.union(item.get_parent_set())
        return parent_set

    def search_for_parent_key(self, key):
        found = False
        if key is None:
            return found
        key=key.lower()
        parent_set = self.get_parent_set()
        return (key in parent_set)


class LinkMessage(object):
    def __init__(self,depth, link_object=LinkObject()):
        self.depth = depth
        self.link_object=link_object
        self.count=0


class LinkQueue(AbstractLinkQueue):

    def __init__(self):
        super(LinkQueue, self).__init__()
        self.queue = []

    def add_to_queue(self, func_ob : LinkMessage, signal=True):
        if not signal:
            return
        with self.lock:
            self.queue.append(func_ob)

    def prepend_to_queue(self, func_ob : LinkMessage, signal=True):
        #only done when found...don't really care about ignal..
        with self.lock:
            # put it first...
            self.queue.insert(0, func_ob)

    def pop_from_queue(self, random=False, ascending=True):
        ob = None

        with self.lock:
            if not ascending:
                ix = -1
            else:
                ix = 0
            if len(self.queue) > 0:
                ob = self.queue.pop(ix)
        return ob

    def empty(self):
        empt = False
        with self.lock:
            empt = (len(self.queue) == 0)
        return empt

    def less_than(self, num):
        ret = True
        with self.lock:
            ret = (len(self.queue) < num)
        return ret

    def size(self):
        sz = -1
        with self.lock:
            sz = len(self.queue)
        return sz

    def get_up_to(self, num=10):
        if num is None or num < 1:
            num = 10
        try:
            num = int(num)
        except:
            num = 10

        arr = None
        with self.lock:
            arr = self.queue[:num]
            self.queue = self.queue[num:]
        return arr


class LinkStorageObject(object):

    def __init__(self, storage = None):
        if storage is None:
            self.storage = {}
        else:
            self.storage = storage

        self.general_lock = RLock()

    def has_key(self,key):
        ret=False
        with self.general_lock:
            if key.lower() in self.storage:
                ret=True
        return ret

    def valid_link_object(self, link_object):
        if link_object is None or link_object.display is None:
            return False
        return True

    def insert(self, link_object, no_overwrite=True, signal=True):
        if not self.valid_link_object(link_object) or not signal or (no_overwrite and self.has_key(link_object.display)):
            return False
        with self.general_lock:
            self.storage[link_object.display.lower()] = link_object
        return True

    def attach_inbound_link(self, target_key=None, target_link = None, inbound_link=None, inbound_key = None):
        if target_link is None:
            if target_key is None or not self.has_key(target_key):
                return False
            target_link = self.storage[target_key.lower()]

        if inbound_link is None:
            if inbound_key is None or not self.has_key(inbound_key):
                return False
            inbound_link = self.storage[inbound_key.lower()]

        self.insert(target_link)
        self.insert(inbound_link)
        return target_link.add_inbound(inbound_link)

    def get(self, key):
        key = key.lower()
        if key in self.storage:
            return self.storage[key]
        return None

def pickle_object(name='cache.pkl', object_to_pickle=None):
    try:
        with open(name,'wb') as f:
            pickle.dump(object_to_pickle,f)
        return True
    except:
        traceback.print_exc()

def unpickle_object(name='cache.pkl'):
    ob = None
    try:
        with open(name,'rb') as f:
            ob = pickle.load(f)
    except:
        print("File '%s' not present, will create new LinkStorageObject"%name)

    return ob


if __name__ == '__main__':
    #test the LinkStorageObject

    #lso = LinkStorageObject()
    import json
    lso = unpickle_object()
    if lso is None:
        print("Pickled storage object None...creating new")
        lso = LinkStorageObject()
    else:
        print(json.dumps(lso.get("The Wailers").get_paths()))

    bob = LinkObject(url='https://bob.com',display = 'Bob')

    marley = LinkObject(url='https://marley.com', display= "Marley")

    wailers = LinkObject(url="https://andthewailers.com", display = "The Wailers")
    print(lso.insert(bob))
    print(lso.insert(marley))
    print(lso.insert(wailers))
    lso.attach_inbound_link(inbound_key='Bob', target_key='Marley')
    lso.attach_inbound_link(inbound_key='Marley', target_key='The Wailers')

    #print(json.dumps(lso.get("Wailers").get_paths()))
    print(json.dumps(lso.get("The Wailers").get_paths(link_trim=8)))
    print(json.dumps(lso.get("The Wailers").get_paths(top_term = 'Marley')))
    pickle_object(name='cache.pkl', object_to_pickle=lso)



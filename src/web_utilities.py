from urllib.request import urlopen
from threading import Thread, RLock
from data_objects import LinkQueue, LinkStorageObject, LinkObject, LinkMessage, Averager, AtomicCounter, ProcessCounter
import traceback
from config_object import ConfigObject, default_config_object
import time
from bs4 import BeautifulSoup
import random


unique_display_set = set()
unique_set_lock = RLock()


def is_in_unique_set(key):
    ret = False
    with unique_set_lock:
        ret = (key in unique_display_set)
    return ret


def insert_in_unique_set(key):
    with unique_set_lock:
        unique_display_set.add(key)


def matches_target_term(link_object: LinkObject, target_term : str):
    if target_term is None or link_object is None:
        return False

    if link_object.url is not None:
        if convert_term_to_wikipedia_entry(target_term) in link_object.url or target_term in link_object.url:
            return True

    return False


def convert_term_to_wikipedia_entry(term):
    return term.replace(' ','_')


def convert_wikipedi_entry_to_term(wikipedia_entry):
    return wikipedia_entry.replace('_',' ')


def create_initial_link(term, base_url_format, base_url_service):
    return base_url_format % (base_url_service,convert_term_to_wikipedia_entry(term))


def create_subordinate_link(sub_url_format, link_in_page):
    return sub_url_format % link_in_page


depth_averager = Averager()
depth_report_count = AtomicCounter()


class SoupTagMatcher(object):
    def __init__(self,config_object=default_config_object,signal=None):
        self.config_object = config_object
        self.service="/%s"%self.config_object.base_url_service
        self.service = self.service.lower()
        self.signal = signal

    def do_get_link_objects(self, html_content, source=None):
        if self.signal is not None and not self.signal.get():
            self.config_object.debug_print("[SoupTagMatcher]:signal flipped, time to stop")
            return []
        soup = BeautifulSoup(html_content,"html.parser")
        div = soup.select('div#bodyContent')[0]
        if div is None:
            div = soup.select('div#bodycontent')
            if div is None:
                return []

        links = div.find_all('a',href=True)
        self.config_object.debug_print("len(links):%d"%len(links))
        arr=[]
        for link in links:
            self.config_object.debug_print("Link---------->%s"%link)
            self.config_object.debug_print("Link(href)-->%s"%link['href'])
            self.config_object.debug_print("Link(text)-->%s"%link.text)
            try:
                hyperlink = link['href']
            except:
                hyperlink = None

            self.config_object.debug_print("hyperlink:%s"%hyperlink)
            name = link.text
            if is_in_unique_set(name):
                self.config_object.debug_print("[SoupPatternMatch] '%s' in unique set!!"%name)
                continue
            bad = False
            if hyperlink is None:
                bad = True
            else:
                self.config_object.debug_print("str(hyperlink).lower():%s"%str(hyperlink).lower())
                if str(hyperlink).lower().startswith(self.service):
                    hyperlink = create_subordinate_link(self.config_object.subordinate_url_format, hyperlink)
                    self.config_object.debug_print("created hyperlink:%s"%hyperlink)

                if self.config_object.url_match_strings is not None:

                    for url_match_string in self.config_object.url_match_strings:

                        if not str(hyperlink).lower().__contains__(url_match_string.lower()):
                            self.config_object.debug_print("link did not match url match string(%s)"%url_match_string)
                            bad=True
                            break
                    if name is None or name == '':
                        bad=True
                        self.config_object.debug_print("link label empty..")
                    if source is not None and name.lower() == source.lower():
                        bad=True
                        self.config_object.debug_print("recursive link found...")
            if bad:
                continue

            new_link_object = LinkObject(url=hyperlink,display=name)
            arr.append(new_link_object)
            insert_in_unique_set(name)
        return arr

    def get_link_objects(self,html_content,source=None):
        try:
            return self.do_get_link_objects(html_content,source)
        except:
            self.config_object.debug_print(traceback.format_exc(5))
        return []


class Crawler(Thread):
    def __init__(self, start_term, end_term, crawler_queue=LinkQueue(),
                 link_storage = LinkStorageObject(),config_object=default_config_object,index=0,
                 signal=ProcessCounter(), start_time = None,reverse=False):
        super(Crawler, self).__init__()
        #self.tag_matcher = TagMatcher(config_object=config_object)
        self.tag_matcher = SoupTagMatcher(config_object=config_object,signal=signal)
        self.crawler_queue = crawler_queue
        self.link_storage= link_storage
        self.config_object = config_object
        self.index=index
        if reverse:
            self.crawlername= "Crawler[reversed](%d)"%index

        else:
            self.crawlername = "Crawler(%d)"%index
        self.start_term = start_term
        self.end_term = end_term
        self.signal = signal
        self.depth = 0
        self.read_depth = 0
        if reverse:
            self.ascending=False
        else:
            self.ascending=True

        if reverse:
            self.initial_jitter= random.uniform(5.1,15.1)
        else:
            self.initial_jitter=random.uniform(10.1,20.1)

        self.start_time = start_time
        self.personal_depth_count = 0

    def get_message(self, message):
        return "[%s]:%s"%(self.crawlername, message)

    def print_message(self, message):

        self.config_object.debug_print(self.get_message(message))

    def check_for_start_end_path(self):
        ret = False
        if self.link_storage.has_key(self.end_term):
            end_obj = self.link_storage.get(self.end_term)
        else:
            end_obj = None

        if end_obj is not None:
            ret=end_obj.search_for_parent_key(self.start_term)

        return ret

    def run(self):
        self.print_message("Entering run method.")
        sleep_amt = self.config_object.crawler_empty_sleep
        consec_empties = 0
        self.print_message("Sleeping for %2f secs"%self.initial_jitter)
        time.sleep(self.initial_jitter)
        self.print_message("...ready to enter queue read loop")
        crawler_queue_empty = False
        crawler_queue_count = -1

        while self.signal.get() and depth_averager.average() < self.config_object.max_depth:  # or not crawler_queue_empty:
            crawler_queue_count, crawler_queue_empty = self.crawler_queue.count_and_empty()
            if self.config_object.queue_status_report and self.depth > 0:
                depth_report_count.increment()
                extra = ", avg depth:%2f, crawlers:%d"%(depth_averager.average(), self.signal.count())

                if self.start_time is not None:
                    dur = time.time() - self.start_time
                else:
                    dur = None
                if dur is not None:
                    print("\033[2J\033[HQueue empty?:%s(size:%d), signal:%s, read depth:%d, write depth:%d%s... elapsed(secs):%2f"%(crawler_queue_empty, crawler_queue_count,self.signal.get(),self.read_depth,self.depth,extra,dur))
                else:
                    print("\033[2J\033[HQueue empty?:%s(size:%d), signal:%s, read depth:%d, write depth:%d%s"%(crawler_queue_empty, crawler_queue_count,self.signal.get(),self.read_depth,self.depth,extra))
            if self.link_storage.has_key(self.end_term):
                done_by_search=self.check_for_start_end_path()
                if done_by_search:
                    self.print_message("Done by search!")
            else:
                done_by_search = False

            if self.config_object.term_when_found and done_by_search:
                self.config_object.debug_print("Found end term '%s' --> setting signal to false"%self.end_term)
                self.signal.notify(term=True)

            signal_value = self.signal.get()
            self.print_message("Signal:%s <--> Depth:%d"%(signal_value,self.depth))
            # if self.depth > self.config_object.max_depth:
            #     self.personal_depth_count += 1
            #     if self.personal_depth_count > 25:
            #         self.print_message("Reached depth:%d 10 times.."%self.depth)
            #         self.signal.notify()

            msg = self.crawler_queue.pop_from_queue(ascending=self.ascending)
            if msg is None:
                sleep_amt = self.config_object.crawler_empty_sleep
                consec_empties+=1
                if consec_empties > self.config_object.acceptable_consecutive_empties:
                    self.signal.notify()
            else:
                depth_averager.add_value(msg.depth)
                consec_empties = 0
                ob = msg.link_object
                self.read_depth=msg.depth
                if self.read_depth > self.config_object.max_depth:
                    continue
                self.depth=msg.depth + 1
                #ob is a LinkObject,depth is new depth for child links
                sleep_amt = self.config_object.crawler_std_sleep
                new_insert = self.link_storage.insert(ob,signal=signal_value)
                if ob.display.lower() == self.end_term.lower():
                    #found it!
                    self.print_message("Found term:%s"% self.end_term)
                    if self.config_object.term_when_found:
                        self.signal.notify(term=True)

                if not self.signal.get():
                    continue

                try:
                    page = urlopen(ob.url)
                    html = page.read().decode('utf-8')

                    for item in self.tag_matcher.get_link_objects(html,ob.display):
                        mtt = False
                        if matches_target_term(link_object=item, target_term=self.end_term):
                            mtt = True
                            self.print_message("Found term '%s' --> %s"%(self.end_term, item.url))
                        succ , addmsg =item.add_inbound(ob)
                        if not succ:
                            self.print_message(addmsg)
                        ob.add_child(item.display)
                        if mtt:
                            self.crawler_queue.prepend_to_queue(func_ob = LinkMessage(depth=self.depth, link_object=item), signal=self.signal.get())
                        else:
                            self.crawler_queue.add_to_queue(func_ob=LinkMessage(depth=self.depth,link_object=item),signal=self.signal.get())

                except:
                    self.print_message("Error reading url:%s -->%s"%(ob.url,traceback.format_exc(5)))
                    if msg.count < 5:
                        msg.count += 1
                        self.crawler_queue.add_to_queue(func_ob=msg, signal=self.signal.get())
                    else:
                        ob.link_valid = False

            time.sleep(sleep_amt)
        self.print_message("Leaving run method")


if __name__ == '__main__':
    print("I love new york"[0:30])
    print("I love new york"[0:10])
    #functional test of thing
    url = "http://olympus.realpython.org/profiles/dionysus"
    page = urlopen(url)
    html = page.read().decode("utf-8")
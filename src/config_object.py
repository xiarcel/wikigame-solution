import json
import traceback
from threading import RLock


def val_or_default(cfmap, key,val):

    if key in cfmap:
        return cfmap[key]

    subkey = None
    if '.' in key:
        lst=key.split('.')
        key=lst.pop(0)
        subkey='.'.join(lst)
        #print "key:%s, subkey:%s"%(key,subkey)

        if key in cfmap:
            ob=cfmap[key]
            return val_or_default(ob,subkey,val)

    return val


def val_or_none(cfmap, key):
    return val_or_default(cfmap, key, None)


def val_or_error(cfmap, key, error_form="Value not found! (%s)"):
    ob = val_or_none(cfmap,key)
    if ob is None:
        raise Exception(error_form%key)
    return ob


def int_or_def(int_val,deflt):
    try:
        return int(int_val)
    except:
        pass
    return deflt


def float_or_def(float_val,deflt):
    try:
        return float(float_val)
    except:
        pass
    return deflt


def int_val_or_default(cfmap, key, deflt):
    return int_or_def(val_or_default(cfmap,key,deflt),deflt)


def float_val_or_default(cfmap, key, deflt):
    return float_or_def(val_or_default(cfmap,key,deflt),deflt)

class ConfigObject(object):
    def __init__(self, cfile='config.json'):
        init_ex_message=None
        try:
            with open(cfile,'r') as f:
                cfmap = json.load(f)
        except:
            init_ex_message=traceback.format_exc(10)
            cfmap= {}
        self.filename = cfile
        self.max_depth = int_val_or_default(cfmap, 'max_depth', 15)
        self.use_cache = val_or_default(cfmap, 'cache.enabled',False)
        self.cache_filename = val_or_default(cfmap, 'cache.filename','cache.pkl')
        self.crawler_count = int_val_or_default(cfmap, 'crawler.count',1)
        self.crawler_reverse_count = int_val_or_default(cfmap, 'crawler.reverse_count',0)
        self.crawler_empty_sleep = float_val_or_default(cfmap, 'crawler.empty_sleep',2.0)
        self.crawler_std_sleep = float_val_or_default(cfmap,'crawler.std_sleep',1.0)
        #self.indexer_count = int_val_or_default(cfmap, 'indexer.count',1)
        self.indexer_empty_sleep = float_val_or_default(cfmap, 'indexer.empty_sleep',2.5)
        self.indexer_std_sleep = float_val_or_default(cfmap, 'indexer.std_sleep',1.2)
        self.debug= val_or_default(cfmap,'debug',False)
        if init_ex_message is not None:
            self.debug_print(init_ex_message)

        self.term_when_found = val_or_default(cfmap,'crawler.term_when_found',False)
        self.url_match_strings = val_or_none(cfmap, 'crawler.url_match_strings')
        self.acceptable_consecutive_empties = val_or_default(cfmap,'crawler.acceptable_consecutive_empties',20)
        self.display_trim_length = val_or_none(cfmap,'display_trim_length')
        self.base_url_format = val_or_default(cfmap,'base_url_format','https://test.com/wiki/%s')
        self.search_for_body_content = val_or_default(cfmap, 'crawler.limit_to_body_content',True)
        self.base_url_service=val_or_default(cfmap,'base_url_service','wiki')
        self.subordinate_url_format = val_or_default(cfmap,'subordinate_url_format','https://test.com/%s')
        self.add_default_logger = val_or_default(cfmap,'add_default_logger',False)
        self.queue_status_report = val_or_default(cfmap,'queue_status_report',False)
        self.default_logger=None

    def debug_print(self, msg):
        if self.default_logger is not None:
            self.default_logger.debug(msg)
        elif self.debug:
            print(msg)


default_config_object = ConfigObject()




if __name__ == '__main__':
    print("cache_filename:%s"%default_config_object.cache_filename)
    print("use_cache:%s"%default_config_object.use_cache)
    print("max_depth:%d"%default_config_object.max_depth)
    print("crawler_count:%d"%default_config_object.crawler_count)
    print("debug:%s"%default_config_object.debug)
    print("base_url_format:%s"%default_config_object.base_url_format)
    print("subordinate_url_format:%s"%default_config_object.subordinate_url_format)
import logging
import logging.handlers


def create_rotating_file_handler(fname, mode='a', maxBytes=80000000, backupCount=1):
    return logging.handlers.RotatingFileHandler(filename=fname, mode=mode,maxBytes=maxBytes,backupCount=backupCount)


#these defaults will be used (formatter/handler/log level) for get logger calls.
PATTERN="{%(asctime)s %(levelname)s:%(name)s}:%(message)s"
DEFAULT_FORMATTER=logging.Formatter(PATTERN)
DEFAULT_HANDLER = create_rotating_file_handler(fname='./wiki_game.log')
DEFAULT_LOG_LEVEL=logging.DEBUG


def set_default_log_level(ll):
    global DEFAULT_LOG_LEVEL
    DEFAULT_LOG_LEVEL=ll


def set_default_handler(hndl):
    global DEFAULT_HANDLER
    DEFAULT_HANDLER=hndl


def set_default_formatter(fmt):
    global DEFAULT_FORMATTER
    DEFAULT_FORMATTER=fmt


def compute_log_level(llstr):
    if llstr is None:
        return DEFAULT_LOG_LEVEL
    llstr = str(llstr).lower()
    if llstr == 'warn' or llstr == 'warning':
        return logging.WARNING
    if llstr == 'info':
        return logging.INFO
    if llstr == 'error':
        return logging.ERROR
    if llstr == 'debug':
        return logging.DEBUG

    get_logger(name="logging_config_internal").error("No value for llstr '%s' will use default (probably info)"%llstr)
    return DEFAULT_LOG_LEVEL


def get_logger(name='root',handler=DEFAULT_HANDLER,formatter=DEFAULT_FORMATTER,lvl = DEFAULT_LOG_LEVEL):
    logger=logging.getLogger(name)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(lvl)
    return logger

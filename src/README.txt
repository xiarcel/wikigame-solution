Instructions:
    Written for python3...mostly py2 compatible...but it was tested with python3
    execute:
        python3 wikipedia_game.py "First term" "Second term"
        (ex.
            python3 wikipedia_game.py "Web Bot" "Tax holiday"
Strategy:
    I decided on a strategy of little to no non builtin libraries as a way to demonstrate how I worked through pythib to build a
    solution to the problem as stated in The_Wikipedia_game.txt.

    BeautifulSoup came in handy :)



Basic features:
    1. Configurable (to a degree) with a config filee
    2. Optional (config.json I've included does this...) cache function using a pickle to save already attacheed links.
    3. Optional debug flag in config to see way more output



TO DO:
    1. Strip out debug_print calls and replace with full fledged loggers
    2. Figure out a way for it to run faster... the test case:
        python3 wikipedia_game.py "Web Bot" "Tax holiday"

       It takes quite some time.

